# WSO2 API Manager Charts

~~~sh
$ helm upgrade --install wso2-micro-integrator ./ -n wso2 

# Caso deseja testar antes de aplicar definitivamente use:
--dry-run
~~~

## Instalando

~~~sh
$ kubectl create secret docker-registry wso2micro-integrator-deployment-creds --docker-server=docker.wso2.com --docker-username=deusimardosanjos@gmail.com --docker-password= --docker-email=deusimardosanjos@gmail.com --namespace wso2

$ helm upgrade --install micro-integrator wso2/micro-integrator --version 1.2.0-2 --namespace wso2

$ helm uninstall micro-integrator -n wso2
~~~