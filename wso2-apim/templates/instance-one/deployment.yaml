apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "wso2-apim.fullname" . }}
  labels:
    {{- include "wso2-apim.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "wso2-apim.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "wso2-apim.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "wso2-apim.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
        - name: init-mysql-connector-download
          image: busybox:1.32
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - /bin/sh
            - "-c"
            - |
              set -e
              connector_version=8.0.17
              wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/${connector_version}/mysql-connector-java-${connector_version}.jar -P /mysql-connector-jar/
          volumeMounts:
            - name: mysql-connector-jar
              mountPath: /mysql-connector-jar
        - name: init-am-analytics-worker
          image: busybox:1.32
          command: ['sh', '-c', 'echo -e "Checking for the availability of WSO2 API Manager Analytics Worker deployment"; while ! nc -z {{ include "wso2-apim.fullname" . }}-analytics-worker 7712; do sleep 1; printf "-"; done; echo -e "  >> WSO2 API Manager Analytics Worker has started";']
        {{/*
            Essa verificação na inicialização deve ser aplicado somente nas instancias adicionais.
        - name: init-api-manager
          image: busybox:1.32
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ['sh', '-c', 'echo -e "Checking for the availability of WSO2 API Manager instance one deployment"; while ! nc -z {{ include "wso2-apim.fullname" . }} 9443; do sleep 1; printf "-"; done; echo -e "  >> WSO2 API Manager instance one has started";']
        */}}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: JVM_MEM_OPTS
              value: "-Xms1024m -Xmx1024m"
          ports:
            - containerPort: 8280
              protocol: "TCP"
            - containerPort: 8243
              protocol: "TCP"
            - containerPort: 9443
              protocol: "TCP"
            - containerPort: 9763
              protocol: "TCP"
            - containerPort: 9711
              protocol: "TCP"
            - containerPort: 9611
              protocol: "TCP"
            - containerPort: 5672
              protocol: "TCP"
          livenessProbe:
            exec:
              command:
                - /bin/sh
                - -c
                - nc -z localhost 9443
            initialDelaySeconds: 360
            periodSeconds: 10
            failureThreshold: 3
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
                - /bin/sh
                - -c
                - nc -z localhost 9443
            initialDelaySeconds: 360
            periodSeconds: 10
            failureThreshold: 3
            successThreshold: 1
            timeoutSeconds: 1
          lifecycle:
            preStop:
              exec:
                command:  ['sh', '-c', '${WSO2_SERVER_HOME}/bin/wso2server.sh stop']
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: wso2am-executionplans-storage
              mountPath: /home/wso2carbon/wso2am-3.2.0/repository/deployment/server/executionplans
            - name: wso2am-synapse-configs-storage
              mountPath: /home/wso2carbon/wso2am-3.2.0/repository/deployment/server/synapse-configs
            - name: wso2am-conf
              mountPath: /home/wso2carbon/wso2-config-volume/repository/conf
            - name: mysql-connector-jar
              mountPath: /home/wso2carbon/wso2-artifact-volume/repository/components/dropins
      volumes:
        - name: wso2am-executionplans-storage
          persistentVolumeClaim:
            claimName: {{ include "wso2-apim.fullname" . }}-shared-executionplans
        - name: wso2am-synapse-configs-storage
          persistentVolumeClaim:
            claimName: {{ include "wso2-apim.fullname" . }}-shared-synapse-configs
        - name: wso2am-conf
          configMap:
            name: {{ include "wso2-apim.fullname" . }}-conf
        - name: mysql-connector-jar
          emptyDir: {}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
